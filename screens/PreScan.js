import React, {Component} from 'react';
import { StyleSheet, Text, View , TouchableOpacity, Image, Dimensions, ImageBackground, Button } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Colors from '../constants/Colors'
import { ImageManipulator } from 'expo-image-crop'
import * as compressManipulator from "expo-image-manipulator";

const { width, height } = Dimensions.get('window')

export default class PreScan extends Component {
    constructor(props) {
        super(props)
        this.reload = this.reload.bind(this);
        this.scanImage = this.scanImage.bind(this);
        this.compress = this.compress.bind(this);
        this.state = {
          isVisible: false,
          uri: this.props.route.params.url
        }
    }

    reload() {
        this.setState({ image: null })
        this.props.navigation.navigate('Home')
    }

    scanImage() {
        const { uri } = this.state
        this.compress(uri).then(() => {
          this.props.navigation.navigate('FeedBack', { url: uri });
        });
    }

    compress = (image) => {
      return compressManipulator.manipulateAsync(
        image, [], { compress: 0.2, format: compressManipulator.SaveFormat.JPEG }
      ).then(result => {
        this.setState({ uri: result.uri });
      })
      .catch(error => {
          console.log(error)
      }) 
    };

    onToggleModal = () => {
      const { isVisible } = this.state
      this.setState({ isVisible: !isVisible })
    }

    render() {
      const { isVisible, uri } = this.state
        return (
              <View style={{zIndex: 1, flex: 1}}>
                <ImageBackground source={{ uri }} resizeMode="contain" style={{
                  justifyContent: 'center', padding: 20, alignItems: 'center', height, width, backgroundColor: 'black'}}>
                  <ImageManipulator
                    photo={{ uri }}
                    isVisible={isVisible}
                    onPictureChoosed={({ uri: uriM }) => this.setState({ uri: uriM })}
                    onToggleModal={this.onToggleModal} />
                </ImageBackground>
                    <TouchableOpacity
                      style={{left: 20, top: 50, zIndex: 4, position: 'absolute'}}
                      onPress={this.reload}>
                      <FontAwesome name="times" style={{ color: "#fff", fontSize: 30}}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{left: 20, top: 820, zIndex: 4, position: 'absolute'}}
                      style={styles.editContainer}
                      onPress={() => this.setState({ isVisible: true })}>
                    <Text style={styles.buttonText}>Edit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.buttonContainer}
                      onPress={this.scanImage}>
                    <Text style={styles.buttonText}>Scan <FontAwesome name="paper-plane" style={{ color: "#fff", fontSize: 25}}/></Text>
                    </TouchableOpacity>
              </View>
     )}
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 3,
  },
  buttonContainer: {
    position: 'relative',
    width: 110,
    backgroundColor: Colors.allergensCare,
    borderRadius: 7,
    padding: 8,
    paddingLeft: 12,
    bottom: 90,
    left: 280
  },
  editContainer: {
    position: 'absolute',
    width: 65,
    backgroundColor: Colors.allergensCare,
    borderRadius: 7,
    padding: 8,
    paddingLeft: 12,
    top: 60,
    left: 320
  },
  buttonText: {
    fontSize: 25,
    color: '#fff'
  }
})