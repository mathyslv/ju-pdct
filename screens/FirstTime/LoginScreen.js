import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, View, Image, TextInput, Dimensions, KeyboardAvoidingView, Animated } from 'react-native';
import { connect } from "react-redux";
import BaseButton from '../../components/BaseButton';
import BaseContainer from '../../components/BaseContainer';
import BaseFitImage from '../../components/BaseFitImage';
import { addUser, setFirstTime } from '../../store/actions';
import logo from '../../assets/images/Profil.jpg'
import titleSrc from '../../assets/images/title001.png';
import Colors from '../../constants/Colors'
import { ScreenNames } from "../../constants/Screens";

const { width: WIDTH } = Dimensions.get('window')

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.onImageLoad = this.onImageLoad.bind(this)
    this.fadingIn = new Animated.Value(0);
    this.fadingOut = new Animated.Value(1);
    this.login = this.login.bind(this);
    this.state = {
      username: this.props.user.username,
      gifLoaded: false
      //familyName: this.props.user.familyName
    }
  }

  onImageLoad () {
    Animated.timing(this.fadingIn, {
      toValue: 1,
      duration: 250,
      delay: 1000
    }).start(({finished}) => {
    });


    Animated.timing(this.fadingOut, {
      toValue: 0,
      duration: 1000,
    }).start(() => {
      this.setState({ gifLoaded: true })
    })
  }

  login() {
    const test = {
      username: this.state.username,
      //familyName: this.state.familyName
    }
    this.props.addUser(test);
    this.props.navigation.navigate(ScreenNames.Allergens);
  }

  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} behavior="padding" enabled>
      <BaseContainer>
        <View style={{ flex: 3, padding: 45 }}>
          <BaseFitImage source={titleSrc} />
        </View>
        <View style={{ flex: 3, alignItems: "center", paddingBottom: 45}}>
          <View style={{ flex: 1, width: WIDTH * 0.75, alignItems: "center" }}>
            {this.state.gifLoaded ||
            <Animated.Text style={{...styles.loadingText, opacity: this.fadingOut}}>
              Loading your avocado ...
            </Animated.Text>
            }
            {this.state.gifLoaded || <ActivityIndicator size="large" color="#FFF" /> }
            <Animated.Image
                onLoad={this.onImageLoad}
                source={logo}
                style={{ ...styles.logo, opacity: this.fadingIn }} />
            {!this.state.gifLoaded || 
            <TextInput
              style={{ ...styles.input,  }}
              placeholder="Your superhero name ? 🚀"
              placeholderTextColor="#999"
              onChangeText={text => this.setState({username: text})}
              value={this.state.username}
              returnKeyType="done"
              />
            }
          </View>
        </View>
        <View style={{ flex: 1, alignItems: "center", paddingHorizontal: 45 }}>
          <BaseButton
            style={{ marginTop: 22.5, width: '100%', opacity: this.fadingIn }}
            block
            text="I want to save the world !"
            onPress={this.login}
          />
        </View>
      </BaseContainer>
      </KeyboardAvoidingView>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
};

const styles = StyleSheet.create({
  loadingText: {
    textAlign: "center",
    fontSize: 25,
    color: "white",
    marginBottom: 25
  },
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    //justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.allergensCare
  },
  container: {
    marginTop: 300
  },
  logoContainer: {
    alignItems: "center",
    marginBottom: 50
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginBottom: 10
  },
  inputContainer: {
    marginTop: 10
  },
  input: {
    height: 40,
    borderRadius: 7,
    fontSize: 16,
    paddingLeft: 10,
    backgroundColor: '#FFFFFF',
    color: '#000000',
    width: 210,
    borderColor: '#000',
    borderWidth: 1,
  }
});

export default connect(mapStateToProps, { addUser, setFirstTime })(LoginScreen);
