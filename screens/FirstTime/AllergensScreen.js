import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import BaseContainer from '../../components/BaseContainer';
import AllergensList from '../../components/AllergensList';
import BaseButton from '../../components/BaseButton';
import Colors from '../../constants/Colors'

export default class AllergensScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={{ flex: 1, paddingTop: 65, paddingBottom: 45, paddingHorizontal: 15 }}>
        <Text style={styles.title}>What are your allergies ?</Text>
        <AllergensList />
        <BaseButton
          text="Save the world !"
          style={styles.submitBtn}
          textStyle={{ color: "white", fontWeight: "bold" }}
          onPress={() => this.props.navigation.navigate('Root', { screen: 'Home'})}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 32,
    color: "black",
    marginBottom: 30
  },
  submitBtn: {
    marginTop: 30,
    backgroundColor: Colors.allergensCare,
  }
})