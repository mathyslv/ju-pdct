import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, View, Text, Image, Dimensions, Animated } from 'react-native';
import BaseButton from '../../components/BaseButton';
import BaseContainer from '../../components/BaseContainer';
import BaseFitImage from '../../components/BaseFitImage';
import titleSrc from '../../assets/images/title001.png';
import avocadoSrc from '../../assets/images/avocado.gif';
import Colors from '../../constants/Colors'

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')

class WelcomeScreen extends Component {

  constructor(props) {
    super(props);
    this.onImageLoad = this.onImageLoad.bind(this)
    this.fadingIn = new Animated.Value(0);
    this.fadingOut = new Animated.Value(1);
    this.state = {
      gifLoaded: false
    }
  }

  onImageLoad () {
    Animated.timing(this.fadingIn, {
      toValue: 1,
      duration: 250,
      delay: 1000
    }).start(({finished}) => {
    });


    Animated.timing(this.fadingOut, {
      toValue: 0,
      duration: 1000,
    }).start(() => {
      this.setState({ gifLoaded: true })
    })
  }

  render() {
    return (
      <BaseContainer>
        <View style={{ flex: 3, padding: 45 }}>
          <BaseFitImage source={titleSrc} />
        </View>
        <View style={{ flex: 3, paddingBottom: 22.5 }}>
        {this.state.gifLoaded ||
          <Animated.Text style={{...styles.loadingText, opacity: this.fadingOut}}>
            Loading the avocados ...
          </Animated.Text>
          }
        {this.state.gifLoaded || <ActivityIndicator size="large" color="#FFF" /> }
          <Animated.Image
            onLoad={this.onImageLoad}
            source={avocadoSrc}
            style={{ ...styles.avocadoImage, opacity: this.fadingIn }} />
        </View>
        <View style={styles.middle}>
          <BaseButton
            style={{ opacity: this.fadingIn }}
            block
            text="Start"
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </View>
      </BaseContainer>
    )
  }
}

const styles = StyleSheet.create({
  loadingText: {
    textAlign: "center",
    fontSize: 25,
    color: "white",
    marginBottom: 25
  },
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: Colors.allergensCare,
    paddingTop: 45,
    paddingBottom: 45,
  },
  titleImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "contain"
  },
  middle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnLogin: {
    width: WIDTH * 0.75,
    height: 45,
    borderRadius: 7,
    fontSize: 16,
    backgroundColor: '#FFF',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowColor: '#000',
    shadowOffset: { height: 1.5, width: 0 },
  },
  textLogin: {
    color: '#000000',
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 12
  },
  avocadoImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "contain"
  }
});

export default WelcomeScreen;
