import React, { Component } from 'react';
import { connect } from "react-redux";
import { StyleSheet, Dimensions, View, Image, Text } from 'react-native';
import BaseContainer from '../components/BaseContainer';
import AllergensList from '../components/AllergensList'
import logo from '../assets/images/Profil.jpg'
import { ScrollView } from 'react-native-gesture-handler';

const { width: WIDTH } = Dimensions.get('window')

class ProfileScreen extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
      <BaseContainer style={{backgroundColor: '#fafafa', paddingTop: 20, paddingBottom: 0 }}>
        <ScrollView>
        <View style={{ flex: 3, alignItems: "center" }}>
          <View style={{ flex: 1, width: WIDTH * 0.75, alignItems: "center" }}>
            <View style={styles.container}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.name}> {this.props.user.username} </Text>
            </View>
          </View>
        </View>
        <Text style={styles.categories}> Informations </Text>
        <Text style={{ textAlign: 'center', fontSize: 20 }}>...</Text>
        <Text style={styles.categories}> Allergens </Text>
        <AllergensList />
        </ScrollView>
      </BaseContainer>
    )
  };
}

const mapStateToProps = state => {
  return {
    user: state.user,
    allergens: state.settings.allergens
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    paddingLeft: 15,
    paddingRight: 15,
  },
  pageTitle: {
    fontSize: 32,
    textAlign: "center",
    marginTop: 25,
    marginBottom: 25
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginBottom: 10
  },
  name: {
    textAlign: "center",
    fontSize: 25,
    color: "black",
    marginBottom: 30
  },
  categories: {
    fontSize: 20,
    color: "black",
    paddingTop: 7,
    paddingBottom: 8,
    backgroundColor: '#9CCC65',
    borderColor: '#000',
    borderWidth: 1,
  }
});

export default connect(mapStateToProps)(ProfileScreen);
