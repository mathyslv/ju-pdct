import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, Animated, ActivityIndicator } from 'react-native';
import BaseContainer from '../components/BaseContainer';
import BaseButton from '../components/BaseButton';
import * as FileSystem from 'expo-file-system';
import { connect } from "react-redux";
import avocadoSuccess from '../assets/images/avocatContent.png';
import avocadofail from '../assets/images/avocatTriste.png';

import _request from '../request.json'
import { black } from 'color-name';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')

class FeedBack extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            result: 'Loading...',
        }
    }

    processAnalyze(data) {
        if (!(data && data.responses && data.responses[0])) {
            //console.log(JSON.stringify(data))
            this.setState({
                result: 'Error while recognizing text.',
                loading: false
            })
            return
        }

        const response = data.responses[0]
        const recognizedText = response.textAnnotations[0].description.toLowerCase()
        const detected = this.props.allergens.filter(allergen => recognizedText.includes(allergen.toLowerCase()))
        const result = detected.length ? 'Don\'t eat ! It contains : ' + detected.join(',') : 'All is good for you Inside !'
        
        this.setState({loading: false, result})
    }

    componentDidMount() {
        FileSystem.readAsStringAsync(this.props.route.params.url, {
            encoding: FileSystem.EncodingType.Base64
        }).then(b64 => {
            const request = { ..._request }
            request.requests[0].image.content = b64
            fetch('https://vision.googleapis.com/v1/images:annotate?key=AIzaSyB90iUuAr3TYUcQGcG3DSCYSNTNizgHfHE', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify(request),
            })
            .then(response => response.json())
            .then(data => this.processAnalyze(data))
            .catch(e => console.log('ERROR:', e));
        })
    }

    render() {
        let image;
        if (this.state.result === 'All is good for you Inside !') {
            image = <Image source={avocadoSuccess} style={styles.logo} />
        } else if (this.state.result.includes('contains')) {
            image = <Image source={avocadofail} style={styles.logo} />
        } else {
            image = <Image/>
        }
        return (
            <BaseContainer style={{alignItems: "center", flexDirection: "column" }}>
                <View style={{ flex: 5, width: WIDTH * 0.75, alignItems: "center", justifyContent: "start"}}>
                    <Image source={{ uri: this.props.route.params.url }} style={styles.logo} />
                </View>

                 <View style={{
                    flex: 5,
                    alignItems: "center",
                    justifyContent: "center",
                    width: WIDTH * 0.75,
                    borderRadius: 7,
                    marginVertical: 15
                }}>
                    {image}
                    <Text style={{ color: "#000", fontSize: 16}}>
                        {this.state.result}
                    </Text>
                </View>

                <View style={{ flexShrink: 1, alignItems: "center", justifyContent: "flex-end" }}>
                    <BaseButton
                            block
                            text="Back To Home !"
                            onPress={() => this.props.navigation.navigate('Home')}
                        />
                </View>
            </BaseContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        allergens: state.settings.allergens
    }
  };

const styles = StyleSheet.create({
    logo: {
      flex: 1,
      width: WIDTH * 0.75,
      resizeMode: "contain",
      borderRadius: 12,
      marginVertical: 10,
    },
    name: {
      textAlign: "center",
      fontSize: 25,
      color: "black",
    }
});

export default connect(mapStateToProps)(FeedBack);
