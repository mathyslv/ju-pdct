import { ADD_USER, FIRST_TIME } from '../actions/actionTypes'

const initialState = {
    username: '',
	//familyName: '',
	isFirstTime: true
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_USER:
			//return {...state, username: action.payload.username, familyName: action.payload.familyName};
			return {...state, username: action.payload.username};
		case FIRST_TIME:
			return {...state, isFirstTime: false};
		default: {
			return state;
		}
	}
};

export default reducer;