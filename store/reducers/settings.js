import { SETTINGS_ADD_ALLERGEN, SETTINGS_REMOVE_ALLERGEN } from '../actions/actionTypes'

const initialState = {
    allergens: []
};

const reducer = (state = initialState, action) => {
	console.log(state)
	switch (action.type) {
		case SETTINGS_ADD_ALLERGEN:
			return {
        ...state,
        allergens: [...state.allergens, action.payload]
			};
			
		case SETTINGS_REMOVE_ALLERGEN:
			const index = state.allergens.findIndex(a => a === action.payload)
			if (index === -1) return state
			return {
        ...state,
        allergens: [...state.allergens.slice(0, index), ...state.allergens.slice(index + 1)]
			};
			
		default: {
			return state;
		}
	}
};

export default reducer;