import { ADD_USER, FIRST_TIME } from './actionTypes'

export const addUser = (data) => {
    return {
        type: ADD_USER,
        payload: data
    };
}

export const setFirstTime = () => {
    return {
        type: FIRST_TIME
    };
}