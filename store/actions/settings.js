import { SETTINGS_ADD_ALLERGEN, SETTINGS_REMOVE_ALLERGEN} from './actionTypes'

export const settingsAddAllergen = (data) => {
    return {
        type: SETTINGS_ADD_ALLERGEN,
        payload: data
    };
}

export const settingsRemoveAllergen = (data) => {
    return {
        type: SETTINGS_REMOVE_ALLERGEN,
        payload: data
    };
}