import { createStore, combineReducers, applyMiddleware } from "redux";
import { persistStore, persistReducer } from 'redux-persist';
import { createLogger } from 'redux-logger';
import { AsyncStorage } from 'react-native';

import userReducer from "./reducers/user";
import settingsReducer from "./reducers/settings";

const persistConfig = {
	key: "root",
	storage: AsyncStorage,
	whitelist: ['user', 'settings']
};

const rootReducer = combineReducers({
	user: userReducer,
	settings: settingsReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
	persistedReducer, applyMiddleware(createLogger())
	);
const persistor = persistStore(store);

export { persistor };
