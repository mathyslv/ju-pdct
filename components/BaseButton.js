import React from "react";
import { StyleSheet, View, TouchableOpacity, Dimensions, Animated } from "react-native";

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')

export default function BaseButton (props) {

  const width = props.block ? WIDTH * 0.75 : null

  return (
    <Animated.View style={{ ...styles.wrapper, width, ...props.style }}>
      <TouchableOpacity onPress={() => props.onPress()}>
        <Animated.Text style={{ ...styles.text, ...props.textStyle }}>{props.text}</Animated.Text>
      </TouchableOpacity>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    width: WIDTH * 0.75,
    height: 40,
    borderRadius: 7,
    fontSize: 16,
    backgroundColor: '#FFF',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowColor: '#000',
    shadowOffset: { height: 1.5, width: 0 },
  },
  text: {
    color: '#000000',
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 10
  },
})