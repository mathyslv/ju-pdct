import React from "react";
import { Image, StyleSheet } from 'react-native';

export default function BaseContainer (props) {
  return (
    <Image source={props.source} style={styles.image} />
  )
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "contain"
  }
})