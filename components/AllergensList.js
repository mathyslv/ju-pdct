import React, { Component } from 'react';
import { connect } from "react-redux";
import { StyleSheet, Text, View, Switch, TouchableHighlight } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Divider } from 'react-native-elements';
import { settingsAddAllergen, settingsRemoveAllergen } from '../store/actions'
import { Allergens } from '../constants/Allergens';
import Colors from '../constants/Colors'

const switchProps = {
  trackColor: {
    false: "#CCC",
    true: Colors.allergensCare
  },
  thumbColor: "#fff"
}

class AllergensList extends Component {
  constructor(props) {
    super(props)
    this.generateSettingsRows = this.generateSettingsRows.bind(this)
    this.onSettingsChange = this.onSettingsChange.bind(this)
    this.SettingRow = this.SettingRow.bind(this)
  }

  onSettingsChange(allergen, val) {
    if (val) this.props.settingsAddAllergen(allergen)
    else this.props.settingsRemoveAllergen(allergen)
  }

  generateSettingsRows () {
    let rows = []
    let i = 0
    for (let allergen of Allergens) {
      rows.push(
        this.SettingRow({
          key: i++,
          name: allergen,
          isEnabled: this.props.allergens.includes(allergen),
          onValueChange: val => this.onSettingsChange(allergen, val)
        })
      )
      rows.push(<Divider key={i++} style={{ height: 1, backgroundColor: Colors.allergensCare, marginTop: 5, marginBottom: 5 }} />)
    }
    rows.pop()
    return rows
  }

  SettingRow({ key, name, isEnabled, onValueChange }) {
    const _onValueChange = val => onValueChange(val);
    return (
      <TouchableHighlight
        key={key}
        onPress={() => _onValueChange(!isEnabled)}
        activeOpacity={1}
        underlayColor="#EEE"
        >
        <View style={styles.settingRow}>
          <Text style={[isEnabled ? styles.settingRowTitleActive : styles.settingRowTitleInactive ]}>
            {name}
          </Text>
          <Switch
            trackColor={switchProps.trackColor}
            thumbColor={true ? switchProps.thumbColor.true : switchProps.thumbColor.false}
            ios_backgroundColor={switchProps.trackColor.false}
            onValueChange={_onValueChange}
            value={isEnabled}
          />
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        {this.generateSettingsRows()}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  settingRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 10,
    paddingBottom: 10,
  },
  settingRowTitleInactive: {
    fontSize: 20,
    paddingLeft: 5,
    color: "#9e9e9e",
  },
  settingRowTitleActive: {
    fontSize: 20,
    paddingLeft: 5,
    color: Colors.allergensCare,
    fontWeight: "bold"
  }
});

const mapStateToProps = state => {
  return {
    allergens: state.settings.allergens
  }
};

export default connect(
  mapStateToProps,
  {
    settingsAddAllergen,
    settingsRemoveAllergen
  }
)(AllergensList);