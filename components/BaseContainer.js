import React from "react";
import { StyleSheet, View, Dimensions } from 'react-native';
import Colors from '../constants/Colors'

export default function BaseContainer (props) {
  return (
    <View style={{...styles.container, ...props.style}}>
      {props.children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: Colors.allergensCare,
    paddingTop: 45,
    paddingBottom: 45,
  }
})