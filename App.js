import React, { useEffect} from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import useLinking from './navigation/useLinking';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store/configureStore';
import { setFirstTime } from './store/actions/user';
import WelcomeScreen from './screens/FirstTime/WelcomeScreen';
import LoginScreen from './screens/FirstTime/LoginScreen';
import AllergensScreen from './screens/FirstTime/AllergensScreen';
import FeedBack from './screens/FeedBack';
import PreScan from './screens/PreScan';
import { ScreenNames } from './constants/Screens';

const Stack = createStackNavigator();

export default function AppWrapper() {

  return (
    <Provider store={store}>
      <App /> 
    </Provider>
  )
}

let isFirstTime;

function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  const _isFirstTime = useSelector(state => state.user.isFirstTime);
  const dispacth = useDispatch();

  useEffect(() => {
    isFirstTime = _isFirstTime; 
    if (_isFirstTime) {
      dispacth(setFirstTime())
    }
  }, [])

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  const firstTimeScreens = [
    {  component: WelcomeScreen, name: ScreenNames.Welcome, title: "Welcome" },
    {  component: LoginScreen, name: ScreenNames.Login, title: "Superhero name" },
    {  component: AllergensScreen, name: ScreenNames.Allergens, title: "What do you want to save" },
  ]

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  } else {
    return (
      <PersistGate loading={null} persistor={persistor}>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" backgroundColor="#6a51ae" />
          <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
            <Stack.Navigator>
            { isFirstTime && firstTimeScreens.map((screen, index) => (
              <Stack.Screen
                key={index}
                name={screen.name}
                component={screen.component}
                options={{ title: screen.title, headerShown: false }}
              />
            ))}
             <Stack.Screen name="Root" component={BottomTabNavigator} />
             <Stack.Screen name="PreScan" component={PreScan} options={{ headerShown: false }} />
             <Stack.Screen name="FeedBack" component={FeedBack} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </PersistGate>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});