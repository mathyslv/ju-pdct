const allergensCare = '#9CCC65'
//const allergensCare = '#afcd96'

const tintColor = allergensCare;

export default {
  allergensCare,
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
